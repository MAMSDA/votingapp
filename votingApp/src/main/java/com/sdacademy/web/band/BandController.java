package com.sdacademy.web.band;

import com.sdacademy.entity.Band;
import com.sdacademy.repository.BandRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedList;
import java.util.List;

@RestController
@RequestMapping("/band")
public class BandController {

    @Autowired
    private BandRepository bandRepository;

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public void addBand(@RequestBody BandDTO band){
        bandRepository.saveAndFlush(band.gDomainBand());
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public @ResponseBody List<BandDTO> getList(){
        List<Band> entityList = bandRepository.findAll();
        List<BandDTO> result = new LinkedList<>();
        for(Band band : entityList){
            result.add(BandDTO.fromBand(band));
        }
        return result;
    }

    @RequestMapping(value = "/{bId}", method = RequestMethod.GET)
    public Band getDetails( @PathVariable("bId")Integer bId) {
        return bandRepository.findOne(bId);
    }

}
