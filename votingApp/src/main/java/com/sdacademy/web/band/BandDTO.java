package com.sdacademy.web.band;

import com.sdacademy.entity.Band;
import com.sdacademy.entity.Track;
import com.sdacademy.web.track.TrackDTO;

import java.util.LinkedList;
import java.util.List;


public class BandDTO {
    private String name;
    private String description;
    private String link;
    private List<TrackDTO> tracklist;


    public BandDTO() {
    }

    public Band gDomainBand() { //stworzyc oddzielna klase util bo nie moze byc getDomainBand gdyz uwaza ze jest pole o takiej nazwie
        Band band = new Band();
        band.setName(name);
        band.setDescription(description);
        band.setLink(link);

        if(tracklist!=null && !tracklist.isEmpty()) {
            List<Track> tracks = new LinkedList<>();
            for (TrackDTO trackDto : tracklist) {
                Track track = trackDto.gTrack();
                tracks.add(track);
                track.setBand(band);
            }
            band.setTrackList(tracks);
        }
        return band;
    }

    public static BandDTO fromBand(Band band){
        BandDTO bandDTO = new BandDTO();
        bandDTO.setName(band.getName());
        bandDTO.setDescription(band.getDescription());
        bandDTO.setLink(band.getLink());
        bandDTO.setTracklist(new LinkedList<>());
        for(Track track: band.getTrackList()){
           bandDTO.getTracklist().add(TrackDTO.fromTrack(track));
        }
        return  bandDTO;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public List<TrackDTO> getTracklist() {
        return tracklist;
    }

    public void setTracklist(List<TrackDTO> tracklist) {
        this.tracklist = tracklist;
    }
}
