package com.sdacademy.web.track;

import com.sdacademy.entity.Track;

public class TrackDTO {
    private String title;
    private int releaseYear;

    public TrackDTO() {
    }

    public Track gTrack() {
        Track track = new Track();
        track.setTitle(title);
        track.setReleaseYear(releaseYear);
        return track;
    }

    public static TrackDTO fromTrack(Track track) {
        TrackDTO trackDTO = new TrackDTO();
        trackDTO.setTitle(track.getTitle());
        trackDTO.setReleaseYear(track.getReleaseYear());
        return trackDTO;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(int releaseYear) {
        this.releaseYear = releaseYear;
    }





}
