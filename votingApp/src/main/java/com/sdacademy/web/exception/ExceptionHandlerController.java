package com.sdacademy.web.exception;

import com.sdacademy.exceptions.UserAlreadyExistsException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
public class ExceptionHandlerController {

        @ResponseStatus(value = HttpStatus.ALREADY_REPORTED)
        @ExceptionHandler(value = UserAlreadyExistsException.class)
        @ResponseBody
        public ExceptionDTO exception(Exception exception, WebRequest request) {
            return new ExceptionDTO(exception.getMessage());
        }
    }

