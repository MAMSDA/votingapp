package com.sdacademy.web.vote;

import com.sdacademy.configuration.security.SecurityContest;
import com.sdacademy.entity.Band;
import com.sdacademy.entity.User;
import com.sdacademy.entity.Vote;
import com.sdacademy.repository.BandRepository;
import com.sdacademy.repository.UserRepository;
import com.sdacademy.repository.VoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/vote")
public class VoteController {

    @Autowired
    private VoteRepository voteRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BandRepository bandRepository;

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public void vote(@RequestBody VoteDTO voteDTO){
        String email = SecurityContest.getEmail();
        String bandName = voteDTO.getBandName();
        User user = userRepository.findUserByEmail(email);
        Band band = bandRepository.findBandByName(bandName);
        Vote vote = new Vote(user, band);
        voteRepository.saveAndFlush(vote);


    }

}
