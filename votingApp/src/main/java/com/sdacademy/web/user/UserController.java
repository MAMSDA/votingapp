package com.sdacademy.web.user;

import com.sdacademy.exceptions.UserAlreadyExistsException;
import com.sdacademy.entity.User;
import com.sdacademy.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public void register(@RequestBody UserDTO userdto) {
        //System.out.println(userdto);

        if (userRepository.findUserByEmail(userdto.getEmail()) == null) {
            userRepository.saveAndFlush(userdto.gDomainUser());
        } else {
            throw new UserAlreadyExistsException();
        }
    }

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public List<User> getList(){
        return userRepository.findAll();
    }

   // @RequestMapping(value = "/login", method = RequestMethod.POST)
    //public User login(@RequestBody LoginRequest loginRequest ){
    //    User userdto =
    //}

}
