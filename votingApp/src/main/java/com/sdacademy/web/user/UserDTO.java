package com.sdacademy.web.user;

import com.sdacademy.entity.User;
import lombok.Data;

import java.util.Date;

@Data
public class UserDTO {

    private String name;
    private String surname;
    private String email;
    private String password;
    private boolean isActive;
    private Date registryDate;

    public UserDTO() {
    }

    public User gDomainUser() {
        User user = new User();
        user.setName(name);
        user.setSurname(surname);
        user.setEmail(email);
        user.setPassword(password);
        user.setActive(false);
        user.setRegistryDate(new Date());
        return user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public Date getRegistryDate() {
        return registryDate;
    }

    public void setRegistryDate(Date registryDate) {
        this.registryDate = registryDate;
    }
}
