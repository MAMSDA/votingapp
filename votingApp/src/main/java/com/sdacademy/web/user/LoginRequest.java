package com.sdacademy.web.user;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class LoginRequest {

    private final String email;
    private final String password;

    @JsonCreator
    public LoginRequest(@JsonProperty(value = "email", required = true) String email,
                        @JsonProperty(value = "password") String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
}
