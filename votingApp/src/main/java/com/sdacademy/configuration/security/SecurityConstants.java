package com.sdacademy.configuration.security;

public final class SecurityConstants {

    public static final String ADMIN = "ADMIN";
    public static final String USER = "USER";
}
