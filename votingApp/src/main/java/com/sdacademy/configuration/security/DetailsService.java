package com.sdacademy.configuration.security;


import com.sdacademy.entity.User;
import com.sdacademy.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.List;

/**
 * Authenticate a user from the database.
 */
@Component("userDetailsService")
public class DetailsService implements UserDetailsService {

    private final Logger log = LoggerFactory.getLogger(UserDetailsService.class);

    @Autowired
    private UserRepository userRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(final String mail) {
        String lowercaseLogin = mail.toLowerCase();
        User userFromDatabase =  userRepository.findUserByEmail(lowercaseLogin);
        List<GrantedAuthority> grantedAuthorities = new ArrayList();
        grantedAuthorities.add (new SimpleGrantedAuthority("user"));


        return new org.springframework.security.core.userdetails.User(mail,
                userFromDatabase.getPassword(), grantedAuthorities);
        }

}
