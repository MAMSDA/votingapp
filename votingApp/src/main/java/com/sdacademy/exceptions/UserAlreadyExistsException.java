package com.sdacademy.exceptions;

import com.sdacademy.entity.User;

public class UserAlreadyExistsException extends RuntimeException {
        public UserAlreadyExistsException() {
            super("User already exists!");
    }
}
