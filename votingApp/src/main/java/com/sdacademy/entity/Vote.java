package com.sdacademy.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table
public class Vote {

    public Vote() {
    }

    public Vote(User user, Band band) {
        this.user = user;
        this.band = band;
    }

    @Id
    @GeneratedValue (strategy = GenerationType.AUTO)
    private Integer vId;

    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "uId")
    private User user;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "bId")
    private Band band;

    private LocalDate date = LocalDate.now();

    public Integer getvId() {
        return vId;
    }

    public void setvId(Integer vId) {
        this.vId = vId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Band getBand() {
        return band;
    }

    public void setBand(Band band) {
        this.band = band;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }
}
