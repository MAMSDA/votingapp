package com.sdacademy.entity;

import lombok.Data;
import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;
import java.util.List;


@Entity
@Table
public class Band {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)

    private Integer bId;
    private String name;
    private String description;
    private String link;

    @Cascade(CascadeType.ALL)
    @OneToMany(mappedBy = "band")
    private List<Vote> voteList;

    @Cascade(CascadeType.ALL)
    @OneToMany(mappedBy = "band" )
    private List<Track> trackList;

    public Band(){}

    public Band(Integer bId, String name, String description, String link, List<Vote> voteList, List<Track> trackList) {
        this.bId = bId;
        this.name = name;
        this.description = description;
        this.link = link;
        this.voteList = voteList;
        this.trackList = trackList;
    }

    public Integer getbId() {
        return bId;
    }

    public void setbId(Integer bId) {
        this.bId = bId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public List<Vote> getVoteList() {
        return voteList;
    }

    public void setVoteList(List<Vote> voteList) {
        this.voteList = voteList;
    }

    public List<Track> getTrackList() {
        return trackList;
    }

    public void setTrackList(List<Track> trackList) {
        this.trackList = trackList;
    }
}
