package com.sdacademy.repository;

import com.sdacademy.entity.Track;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TrackRepository extends JpaRepository<Track, Integer>{

}
