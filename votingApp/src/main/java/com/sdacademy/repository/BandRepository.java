package com.sdacademy.repository;

import com.sdacademy.entity.Band;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface BandRepository extends JpaRepository<Band, Integer>{


    @Query(value = "select b from Band b where b.name=?1")
    Band findBandByName(String name);
}
