package com.sdacademy.repository;

import com.sdacademy.entity.Vote;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VoteRepository extends JpaRepository<Vote, Integer>{
}
