package com.sdacademy.repository;

import com.sdacademy.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.ArrayList;

public interface UserRepository extends JpaRepository<User, Integer>{

    @Query(value = "select u from User u where u.vote.band.name=?1 order by u.vote.date")
    ArrayList<User> findFirstUserVotingForBandByVotingDate(String bandname);

    @Query(value = "select u from User u where u.email=?1")
    User findUserByEmail(String email);


}
