angular.module('sdaapp')
    .controller('BandsController', function ($rootScope, $scope, $http, $location) {
        console.log('Błąd podczas dodawania zespołu.');
        $scope.addBand = function () {
            $scope.bandDetails = {};
            $scope.track = {};
        };



        var trackConstructor = function () {
            var track = {
                title: $scope.track.title,
                releaseYear: $scope.track.releaseYear
            };
            return track;
        }


        $scope.trackList = [];
        $scope.addTrack = function () {
            $scope.trackList.push(trackConstructor());
            $scope.track = {};
            console.log($scope.trackList);
        }

        $scope.registerBand = function () {
            $scope.bandDetails.tracklist = $scope.trackList;
            console.log($scope.bandDetails);
            $http.post('band/add', $scope.bandDetails).success(function (data) {
                console.log(data);
            }).error(function () {
                console.log('Błąd podczas dodawania zespołu.');
                //  $scope.unknownErrorMessage = true;
            });
            $scope.bandDetails = {};
            $scope.trackList = [];
            $scope.bandForm.$setPristine();
            $scope.bandForm.$setUntouched();

        };




    });