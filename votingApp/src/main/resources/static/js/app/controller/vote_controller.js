angular.module('sdaapp')
    .controller('VoteController', function ($rootScope, $scope, $http, $location) {

        $scope.bands = [];
        
        var showBandList = function () {

            $http.get('band/list').success(function (data) {
                console.log(data);

                for (var i=0; i < data.length; i++) {
                    $scope.bands.push(bandConstructor(data[i]));
                }
                
            });
            console.log($scope.bands);
        }

        showBandList();

        var bandConstructor = function (bandObject) {
            var band = {
                name: bandObject.name,
                link: bandObject.link,
                description: bandObject.description,
                tracklist: bandObject.tracklist
            };
            return band;
        }

        var voteConstructor = function (bandName) {
            var vote = {
                bandName: bandName
            }
            return vote;
        }

        $scope.vote = function () {
            var selectedBand = document.querySelector('input[name="voteRadio"]:checked').value;
            console.log("Głosuję na " + selectedBand);
            $scope.vote = voteConstructor(selectedBand);

            $http.post("vote/add", $scope.vote).success(function (data) {
                console.log(data);
            });
        }


        
        

        
        



    });
