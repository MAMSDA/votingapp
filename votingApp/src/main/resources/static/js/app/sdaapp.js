var app = angular.module('sdaapp', ['ngRoute'])
    .config(function($routeProvider) {
        console.log("ngroute");
        $routeProvider.when('/', {
            templateUrl : 'home.html',
            controller : 'HomeController'
        }).when('/login', {
            templateUrl : 'login.html',
            controller : 'NavigationController'
        }).when('/registerr', {
            templateUrl : 'registerr.html',
            controller : 'RegistersController'
        }).when('/bands', {
            templateUrl : 'bands.html',
            controller : 'BandsController'
        }).when('/vote', {
            templateUrl : 'vote.html',
            controller : 'VoteController'
        }).otherwise('/');
    })